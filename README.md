# Data access with JDBC

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

**Spring Boot Application** to access data from the provided Chinook database with JDBC.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Background

This project is a repository implementation to access data from the provided Chinook database with JDBC.

The project also includes separate SQL scripts for creating a Superhero database.


## Install

- Install JDK 17
- Install Intellij
- Clone repository

## Usage

- Run the application

## Maintainers

- [Laszlo Laki](https://gitlab.com/lakilukelaszlo)
- [Adam Olah](https://gitlab.com/adam-olah93)
- [Krisztina Pelei](https://gitlab.com/kokriszti)