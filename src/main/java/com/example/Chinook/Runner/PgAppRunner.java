package com.example.Chinook.Runner;

import com.example.Chinook.Models.Customer;
import com.example.Chinook.Repositories.Customer.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class PgAppRunner implements ApplicationRunner {
    private final CustomerRepository customerRepository;

    @Autowired
    public PgAppRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        System.out.println("-----------------------------------------------------------------------------------------");

        //0. This is a method to test connection to Postgres
        System.out.println("----------------------------------- START 0----------------------------------------------");
        customerRepository.test();
        System.out.println("-------------------------------------END-------------------------------------------------");

        //1. This method returns all the customers can be found in Chinook DB
        System.out.println("----------------------------------- START 1----------------------------------------------");
        System.out.println(customerRepository.findAll());
        System.out.println("-------------------------------------END-------------------------------------------------");

        //2. This method returns customer data for the given customer id
        System.out.println("----------------------------------- START 2----------------------------------------------");
        System.out.println(customerRepository.findById(5));
        System.out.println("--------------------------------------END------------------------------------------------");

        //3. This method returns customer data related to the given first and last name
        System.out.println("----------------------------------- START 3----------------------------------------------");
        System.out.println(customerRepository.findByName("Leonie","Köhler"));
        System.out.println("--------------------------------------END------------------------------------------------");

        //4. This method returns data for a group of customers given by a limit and an offset
        System.out.println("----------------------------------- START 4----------------------------------------------");
        System.out.println(customerRepository.groupOfCustomers(10,10));
        System.out.println("--------------------------------------END------------------------------------------------");

        //5. This method inserts a new customer into Chinook DB
        System.out.println("----------------------------------- START 5----------------------------------------------");
        Customer customer = new Customer(1002, "Jakab", "Buga", "Hungary",
                "9000", "+36509998888", "jakab.buga@yahoo.com");
        System.out.println(customerRepository.insert(customer));
        System.out.println("--------------------------------------END------------------------------------------------");

        //6. This method updates a customer in Chinook DB
        System.out.println("----------------------------------- START 6----------------------------------------------");
        Customer customerUpdate = new Customer(1002, "Jozsi", "Buga", "Hungary",
                "9000", "+36509998888", "jakab.buga@yahoo.com");
        System.out.println(customerRepository.update(customerUpdate));
        System.out.println("--------------------------------------END------------------------------------------------");

        //7. This method returns the name of the country with the most customers
        System.out.println("----------------------------------- START 7----------------------------------------------");
        System.out.println(customerRepository.countryWithMostCustomer());
        System.out.println("--------------------------------------END------------------------------------------------");

        //8. This method returns some data related to customer who spent the most
        System.out.println("----------------------------------- START 8----------------------------------------------");
        System.out.println(customerRepository.highestSpenderCustomer());
        System.out.println("--------------------------------------END------------------------------------------------");

        //9. This method returns the most "popular" genre regarding the customer related to the given customer id
        System.out.println("----------------------------------- START 9----------------------------------------------");
        System.out.println(customerRepository.mostPopularGenre(4));
        System.out.println("--------------------------------------END------------------------------------------------");
    }

}
