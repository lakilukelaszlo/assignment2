package com.example.Chinook.Models;

public record CustomerSpender(int customerId,
                              String firstName,
                              String lastName,
                              double totalSpending
) {}
