package com.example.Chinook.Models;

public record CustomerGenre (int customer_id,
                             String genre,
                             int count){
}
