package com.example.Chinook.Repositories.Customer;

import com.example.Chinook.Models.Customer;
import com.example.Chinook.Models.CustomerCountry;
import com.example.Chinook.Models.CustomerGenre;
import com.example.Chinook.Models.CustomerSpender;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.*;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository{

    private final String url;
    private final String username;
    private final String password;

    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

     /**
     * <p>Get all customers from database</p>
     * @return This method returns all the customers that can be found in Chinook DB
     *
     */
    
    @Override
    public List findAll() {
        String sql = "SELECT * FROM customer";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")

                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

     /**
     * <p>Get customer information corresponding to the given customer id from database</p>
     * @param id  
     * @customer id
     * @return customer data from the given customer id
     */

    @Override
    public Customer findById(Integer id) {
        String sql  = "SELECT * FROM customer WHERE customer_id = ?";
        Customer customer = null;

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            //write prepared statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //set parameter, index starts with 1, not 0!!!
            statement.setInt(1, id);

            //execute prepared statement
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    /**
     * <p>Get customer or customers information corresponding to a given name from the database, name can be lastname or firstname</p>
     * @param firstName String type parameter
     * @param lastName String type parameter
     * @return  This method returns customer data related to the given first and last name
     */

    @Override
    public Customer findByName(String firstName, String lastName) {
        String sql  = "SELECT * FROM customer WHERE first_name LIKE ? AND last_name LIKE ?";
        Customer customer = null;

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            //write prepared statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //set parameter, index starts with 1, not 0!!!
            statement.setString(1, "%" + firstName + "%");
            statement.setString(2, "%" + lastName + "%");

            //execute prepared statement
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

      /**
     * <p>Get information of selected groups of customers from database, get all customers between given id values.</p>
     * @param offset int type id of the first customer
     * @param limit int type id of the last customer
     * @return This method returns data for a group of customers given by a limit and an offset
     */

    @Override
    public Customer groupOfCustomers(int limit, int offset) {
        String sql  = "SELECT * FROM customer LIMIT ? OFFSET ?";
        Customer customer = null;

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            //write prepared statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //set parameter
            statement.setInt(1, limit);
            statement.setInt(2, offset);

            //execute prepared statement
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

     /**
     * <p>Inserts a new customers information into database</p>
     * @param customer Customer record to be inserted
     * @return This method returns a value indicating that the desired operation was completed successfully
     */
   
    @Override
    public int insert (Customer customer) {
        String sql = "INSERT INTO customer (customer_id, first_name, last_name, country, postal_code, phone, email) VALUES (?,?,?,?,?,?,?)";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);

            // Set parameter
            statement.setInt(1, customer.customer_id());
            statement.setString(2, customer.first_name());
            statement.setString(3, customer.last_name());
            statement.setString(4, customer.country());
            statement.setString(5, customer.postal_code());
            statement.setString(6, customer.phone_number());
            statement.setString(7, customer.email());

            // Execute statement
            result = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

     /**
     * <p>Update of customers information</p>
     * @param customer Customer record to be updated
     * @return This method returns a value indicating that the desired operation was completed successfully
     */

    @Override
    public int update(Customer customer) {

        String sql = "UPDATE customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE  customer_id = ?";
        int result = 0;
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);

            // Set parameter
            statement.setString(1, customer.first_name());
            statement.setString(2, customer.last_name());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postal_code());
            statement.setString(5, customer.phone_number());
            statement.setString(6, customer.email());
            statement.setInt(7, customer.customer_id());

            // Execute statement
            result = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * <p>Function returns the name of a country with the most customers</p>
     * @return This method returns the name of the country with the most customers
     */

    @Override
    public CustomerCountry countryWithMostCustomer() {

        String sql = "SELECT country FROM customer GROUP BY country ORDER BY COUNT(*) DESC LIMIT 1";

        CustomerCountry mostCustomers = null;

        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //Execute prepared statement
            ResultSet result = statement.executeQuery();

            // Handle result
            if (result.next()) {
                mostCustomers = new CustomerCountry(result.getString(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mostCustomers;
    }

      /**
     * <p>Returns information of a customer who is the highest spender</p>
     * @return  This method returns some data related to customer who spent the most
     */

    @Override
    public CustomerSpender highestSpenderCustomer() {
        String sql  = "SELECT invoice.total, invoice.customer_id, customer.first_name, customer.last_name "
                + "FROM invoice INNER JOIN customer ON customer.customer_id = invoice.customer_id" +
                " ORDER BY invoice.total DESC LIMIT 1";
        CustomerSpender highestSpender = null;

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            //write prepared statement
            PreparedStatement statement = conn.prepareStatement(sql);

            //execute prepared statement
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                highestSpender = new CustomerSpender(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getDouble("total")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return highestSpender;
    }
    
      /**
     * <p>Finds the most popular genre of a given customer (genre that corresponds to the most tracks the customer had bought)</p>
     * @param customerId int type
     * @return  This method returns the most popular genre or genres(in case of ties) of a customer related to the given customer id
     */

    @Override
    public List<CustomerGenre> mostPopularGenre(int customerId) {
        String sql  = "SELECT customer.customer_id, genre.name, count(genre.name) FROM customer " +
                "INNER JOIN invoice ON customer.customer_id = invoice.customer_id " +
                "INNER JOIN invoice_line ON invoice.invoice_id = invoice_line.invoice_id " +
                "INNER JOIN track ON invoice_line.track_id  = track.track_id " +
                "INNER JOIN genre ON track.genre_id = genre.genre_id " +
                "WHERE customer.customer_id=? "+
                "GROUP BY  customer.customer_id, genre.name ORDER BY  count DESC " +
                "FETCH FIRST 1 ROWS WITH TIES ";

        List<CustomerGenre> popularGenres = new ArrayList<>();
        int count = 0;

        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            //write prepared statement
            PreparedStatement statement = conn.prepareStatement(sql);

            statement.setInt(1, customerId);

            //execute prepared statement
            ResultSet result = statement.executeQuery();

            while (result.next()) {
                CustomerGenre popularGenre = new CustomerGenre(
                        result.getInt("customer_id"),
                        result.getString("name"),
                        result.getInt("count")
                );
                popularGenres.add(popularGenre);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return popularGenres;
    }

    /**
     * <p>Deletes a customers information from database - This method is part of CRUD,
     * but has no relevance in the current situation, therefore no functionality has been given</p>
     * @param customer Customer record to be deleted
     */

    @Override
    public int delete(Customer customer) {
        return 0;
    }

    /**
     * <p>Deletes a customers information from database related to the given customer id - This method is part of CRUD,
     * but has no relevance in the current situation, therefore no functionality has been given</p>
     * @param customerId int type
     */

    @Override
    public int deleteById(Integer customerId) {
        return 0;
    }

    /**
     * <p>This is a function to test connection to Postgres -
     * it prints "CONNECTED TO Postgres..." if connection succeeds</p>
     */

    @Override
    public void test() {

        try (Connection conn = DriverManager.getConnection(url, username, password);) {
            System.out.println("CONNECTED TO Postgres...");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
