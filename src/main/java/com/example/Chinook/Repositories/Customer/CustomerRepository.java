package com.example.Chinook.Repositories.Customer;

import com.example.Chinook.Models.Customer;
import com.example.Chinook.Models.CustomerCountry;
import com.example.Chinook.Models.CustomerGenre;
import com.example.Chinook.Models.CustomerSpender;
import com.example.Chinook.Repositories.CRUDRepository;

import java.util.List;

public interface CustomerRepository extends CRUDRepository<Customer, Integer> {

 //0. This is a method to test connection to Postgres
 void test();

 //3. This method returns customer data related to the given first and last name
 Customer findByName(String firstName, String lastName);

 //4. This method returns data for a group of customers given by a limit and an offset
 Customer groupOfCustomers(int limit, int offset);

 //7. This method returns the name of the country with the most customers
 CustomerCountry countryWithMostCustomer();

 //8. This method returns some data related to customer who spent the most
 CustomerSpender highestSpenderCustomer();

 //9. This method returns the most "popular" genre regarding the customer related to the given customer id
 List<CustomerGenre> mostPopularGenre(int customerId);

}
