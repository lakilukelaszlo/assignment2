INSERT INTO power (power_name, description) VALUES ('flying', 'ability to fly');
INSERT INTO power (power_name, description) VALUES ('web', 'shoot spiderweb from hand');
INSERT INTO power (power_name, description) VALUES ('science', 'ability to invent and make special equipment');

INSERT INTO superhero_power (hero_id, power_id) VALUES (1, 1);
INSERT INTO superhero_power (hero_id, power_id) VALUES (3, 2);
INSERT INTO superhero_power (hero_id, power_id) VALUES (2, 3);
INSERT INTO superhero_power (hero_id, power_id) VALUES (2, 1);