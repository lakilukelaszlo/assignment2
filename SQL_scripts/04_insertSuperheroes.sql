INSERT INTO superhero (superhero_name, alias, origin) VALUES ('Superman', 'Clark Kent', 'Krypton');
INSERT INTO superhero (superhero_name, alias, origin) VALUES ('Batman', 'Bruce Wayne', 'Gotham City');
INSERT INTO superhero (superhero_name, alias, origin) VALUES ('Spiderman', 'Peter Parker', 'New York');